#include <iostream>

int main(){
    int a, b, c;
    std::cout << "Lets do some math\n";
    std::cout << "Enter first number: ";
    std::cin >> a;
    std::cout << "Enter second number: ";
    std::cin >> b;
    std::cout << "Enter third number: ";
    std::cin >> c;
    std::cout << "\n\n";
    int sumOfNumbers = a + b + c;

    std::cout << sumOfNumbers;

    std::cout << "\n\n";

    return 0;
}